import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Client} from '../../../models/client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoggedIn=false;
  client:Client;
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.authService.currentUser.subscribe((data)=>{
      this.isLoggedIn=data!=null;
      this.client=data;
    })
  }

  logout() {
    this.authService.logout();
  }
}
