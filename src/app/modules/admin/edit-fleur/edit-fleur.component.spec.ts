import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFleurComponent } from './edit-fleur.component';

describe('EditFleurComponent', () => {
  let component: EditFleurComponent;
  let fixture: ComponentFixture<EditFleurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFleurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFleurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
