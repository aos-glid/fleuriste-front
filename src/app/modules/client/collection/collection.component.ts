import {Component, OnInit} from '@angular/core';
import {FleurService} from '../../../services/fleur.service';
import {Fleur} from '../../../models/fleur';
import {MatSelectionList, MatSelectionListChange} from '@angular/material/list';
import {AuthService} from '../../../services/auth.service';
import {Client} from '../../../models/client';
import {PanierService} from '../../../services/panier.service';
import {Order} from '../../../models/order';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  loaded = false;
  fleurs: Fleur[] = [];
  selectedFleurs: Fleur[] = [];
  client:Client;
  selection: MatSelectionList;

  constructor(private fleurService: FleurService,private authService:AuthService,private panierService:PanierService) {
  }

  ngOnInit(): void {
    this.authService.currentUser.subscribe((data)=>{
      this.client=data;
      this.fleurService.findAll().subscribe((data) => {
        this.fleurs = data;
        this.loaded = true;
      });
    })

  }

  selectChange($event: MatSelectionListChange) {
    let fleurs: any = $event.source._value;
    this.selectedFleurs = fleurs;
  }

  addToCart() {
    if (this.selectedFleurs.length != 0 && this.client!=null) {
      let orders:Order[]= this.selectedFleurs.map(value => {
        return {fleur:value,quantite:1};
      })
      this.panierService.addToCart(this.client.id, orders);

    }
  }
}
