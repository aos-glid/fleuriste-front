import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClientRoutingModule} from './client-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {HomeComponent} from './home/home.component';
import {CollectionComponent} from './collection/collection.component';
import {FleurCardComponent} from './fleur-card/fleur-card.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PanierComponent } from './panier/panier.component';


@NgModule({
  declarations: [HomeComponent, CollectionComponent,FleurCardComponent, LoginComponent, RegisterComponent, PanierComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule,
  ]
})
export class ClientModule {
}
