import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,private authService:AuthService,private router:Router) {
  }


  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  submit() {
    if (this.formGroup.valid) {
      console.log(this.formGroup.value);
      this.authService.login(this.formGroup.value).subscribe((data)=>{
        if(data!=null)
        {
          this.router.navigate(['/']);
        }
      })
    }
  }
}
