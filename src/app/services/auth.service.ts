import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Client} from '../models/client';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {PanierService} from './panier.service';
const API=environment.url+"/clients";
const USER_KEY = 'auth-user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {


  public currentUser: BehaviorSubject<Client> = new BehaviorSubject<Client>(null);

  constructor(private httpClient:HttpClient,private router:Router,private panierService:PanierService) {
    if(sessionStorage.getItem(USER_KEY)!=null)
    {
      let client = JSON.parse(sessionStorage.getItem(USER_KEY));
      this.currentUser.next(client);
    }
  }

  public login(any):Observable<Client>{
    return this.httpClient.post<Client>(`${API}/login`,any).pipe(map((data)=>{
        if(data!=null)
        {
          window.sessionStorage.removeItem(USER_KEY);
          window.sessionStorage.setItem(USER_KEY, JSON.stringify(data));
          this.currentUser.next(data);
          this.panierService.getPanier(data.id).subscribe();
        }
        return data;
    }));
  }

  public register(any):Observable<any>{
    return this.httpClient.post(`${API}`,any);
  }

  public logout(){
    this.currentUser.next(null);
    window.sessionStorage.clear();
    this.router.navigate(['/']);
  }

  public get(id):Observable<Client>{
    return this.httpClient.get<Client>(`${API}/${id}`);
  }

}
