import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FleurService} from '../../../services/fleur.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-fleur',
  templateUrl: './edit-fleur.component.html',
  styleUrls: ['./edit-fleur.component.scss']
})
export class EditFleurComponent implements OnInit {

  formGroup: FormGroup;
  colors = ['Red', 'Orange', 'Black', 'White', 'Pink', 'Purple', 'Blue', 'Yellow', 'Green', 'Lavender'];
  code: number;
  loaded=false;
  constructor(private formBuilder: FormBuilder, private fleurService: FleurService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.code = this.route.snapshot.params.code;
    this.fleurService.find(this.code).subscribe((data) => {
      this.formGroup = this.formBuilder.group({
        libelle: [data.libelle, Validators.required],
        categorie: [data.categorie, Validators.required],
        couleur: [data.couleur, Validators.required],
        quantite: [data.quantite, Validators.required]
      });
      this.loaded=true;
    });
  }

  submit() {
    this.formGroup.value['code']=this.code;
    console.log(this.formGroup.value);
    if (this.formGroup.valid) {
      this.fleurService.update(this.formGroup.value).subscribe(
        (data) => {
          Swal.fire('Fleur', 'Fleur a ete modifiée avec succees', 'success').then((value) => {
            console.log(value);
            if (value.isConfirmed) {
              this.router.navigate(['admin']);
            }
          });

        }, (error) => {
          Swal.fire('Error', 'Erreur', 'error');
        }
      );
    }
  }

}
