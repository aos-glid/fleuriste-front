import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {FleurService} from '../../../services/fleur.service';
import Swal from "sweetalert2";
@Component({
  selector: 'app-add-fleur',
  templateUrl: './add-fleur.component.html',
  styleUrls: ['./add-fleur.component.scss']
})
export class AddFleurComponent implements OnInit {

  formGroup: FormGroup;
  colors=['Red','Orange','Black','White','Pink','Purple','Blue','Yellow','Green','Lavender'];

  constructor(private formBuilder: FormBuilder,private fleurService:FleurService,private router:Router) {
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      libelle: ['', Validators.required],
      categorie: ['', Validators.required],
      couleur:['',Validators.required],
      quantite:['',Validators.required]
    });
  }

  submit() {
    console.log(this.formGroup.value);
    if (this.formGroup.valid) {
      this.fleurService.add(this.formGroup.value).subscribe(
        (data)=>{
          Swal.fire('Fleur', 'Fleur a ete ajouter avec succees', 'success').then((value) => {
            console.log(value);
            if(value.isConfirmed){
              this.router.navigate(['admin']);
            }
          });

        },(error) => {
          Swal.fire('Error', "Erreur", 'error');
        }
      )
    }
  }

}
