import {Order} from './order';

export class Commande {
    public code:number;
    public date:Date;
    public orders:Order[];
    public client:string;
    public idClient:number;
}
