export class Fleur {
  public code:number;
  public libelle:string;
  public categorie:string;
  public couleur:string;
  public quantite:number;
}
