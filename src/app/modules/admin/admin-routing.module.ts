import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListFleurComponent} from './list-fleur/list-fleur.component';
import {AddFleurComponent} from './add-fleur/add-fleur.component';
import {EditFleurComponent} from './edit-fleur/edit-fleur.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CommandesComponent} from './commandes/commandes.component';

const routes: Routes = [
  {path:'',component:DashboardComponent,children:
  [
    {path:'',component:ListFleurComponent},
    {path:'add',component:AddFleurComponent},
    {path:'commandes',component:CommandesComponent},
    {path:'edit/:code',component:EditFleurComponent}
  ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
