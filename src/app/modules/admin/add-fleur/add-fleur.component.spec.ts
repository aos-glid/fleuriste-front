import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFleurComponent } from './add-fleur.component';

describe('AddFleurComponent', () => {
  let component: AddFleurComponent;
  let fixture: ComponentFixture<AddFleurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFleurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFleurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
