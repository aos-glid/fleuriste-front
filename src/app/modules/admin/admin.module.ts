import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListFleurComponent } from './list-fleur/list-fleur.component';
import { AddFleurComponent } from './add-fleur/add-fleur.component';
import { EditFleurComponent } from './edit-fleur/edit-fleur.component';
import {SharedModule} from '../../shared/shared.module';
import {AdminRoutingModule} from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CommandesComponent } from './commandes/commandes.component';



@NgModule({
  declarations: [ListFleurComponent, AddFleurComponent, EditFleurComponent, DashboardComponent, CommandesComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ],
  bootstrap:[DashboardComponent]
})
export class AdminModule { }
