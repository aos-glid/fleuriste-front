import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,private authService:AuthService,private router:Router) {
  }


  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
    });
  }

  submit() {
    if (this.formGroup.valid) {
      console.log(this.formGroup.value);
      this.authService.register(this.formGroup.value).subscribe((_)=>{

          this.router.navigate(['/login']);
      })
    }
  }
}
