import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Panier} from '../models/panier';
import {map} from 'rxjs/operators';

const API = environment.url + '/paniers';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  public currentPanier: BehaviorSubject<Panier> = new BehaviorSubject<Panier>(null);

  constructor(private httpClient: HttpClient) {
  }

  public getPanier(id: number): Observable<Panier> {
    return this.httpClient.get<Panier>(`${API}/${id}`).pipe(map((data) => {
      if (data != null) {
        this.currentPanier.next(data);
      }
      return data;
    }));
  }

  public addToCart(id: number, orders: any) {
    return this.httpClient.post(`${API}/${id}`, orders).subscribe(_ => {
        this.currentPanier.value.orders.push(orders);
    })
  }

}
