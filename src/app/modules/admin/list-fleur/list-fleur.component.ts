import {Component, OnInit, ViewChild} from '@angular/core';
import Swal from 'sweetalert2';
import {MatTableDataSource} from '@angular/material/table';
import {Fleur} from '../../../models/fleur';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {FleurService} from '../../../services/fleur.service';

@Component({
  selector: 'app-list-fleur',
  templateUrl: './list-fleur.component.html',
  styleUrls: ['./list-fleur.component.scss']
})
export class ListFleurComponent implements OnInit {

  displayedColumns: string[] = ['code','libelle', 'categorie', 'couleur','quantite', 'actions'];
  dataSource: MatTableDataSource<Fleur>;
  public fleurs: Fleur[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private fleurService: FleurService, private router: Router) {
  }

  ngOnInit() {
    console.log("aaa");
    this.fleurService.findAll().subscribe(
      (data) => {
        this.fleurs = data;
        console.log(data);
        this.dataSource = new MatTableDataSource(this.fleurs);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  edit(code: number) {
    this.router.navigate(['/admin/edit', code]);
  }

  delete(code: number) {
    Swal.fire({
      title: 'Deleting Flower ' + code,
      text: 'Are you sure you want to delete this flower ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {

        if (result.isConfirmed) {
          this.fleurService.delete(code).subscribe(
            (data) => {
              this.fleurs=this.fleurs.filter((c)=>c.code!=code);
              this.dataSource = new MatTableDataSource(this.fleurs);
              Swal.fire(
                'Deleted!',
                'Your Flower has been successfully deleted.',
                'success'
              );
            }
          );
        }
      }
    );
  }

}
