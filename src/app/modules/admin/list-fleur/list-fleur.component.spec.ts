import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFleurComponent } from './list-fleur.component';

describe('ListFleurComponent', () => {
  let component: ListFleurComponent;
  let fixture: ComponentFixture<ListFleurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFleurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFleurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
