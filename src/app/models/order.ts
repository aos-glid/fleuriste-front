import {Fleur} from './fleur';

export class Order {
  public fleur:Fleur;
  public quantite:number;
}
