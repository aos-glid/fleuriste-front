import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Fleur} from '../models/fleur';
const API=environment.url+"/fleurs"
@Injectable({
  providedIn: 'root'
})
export class FleurService {

  constructor(private httpClient:HttpClient) { }

  public findAll():Observable<Fleur[]>{
    return this.httpClient.get<Fleur[]>(API);
  }
  public add(fleur:any):Observable<any>{
    return this.httpClient.post(API,fleur);
  }

  public find(code:number):Observable<Fleur>{
    return this.httpClient.get<Fleur>(`${API}/${code}`);
  }

  public delete(code :number):Observable<any>{
    return this.httpClient.delete(`${API}/${code}`);
  }

  public update(data:any):Observable<any>{
    return this.httpClient.put(`${API}`,data);
  }


}
