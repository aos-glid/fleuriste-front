import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {PanierService} from './panier.service';
import {Commande} from '../models/commande';
const API=environment.url+"/commandes";
@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  constructor(private httpClient:HttpClient,private panierService:PanierService) { }

  public findAll():Observable<Commande[]>{
    return this.httpClient.get<Commande[]>(`${API}`);
  }

  public add(commande:any){
    return this.httpClient.post(`${API}`,commande);
  }

}
