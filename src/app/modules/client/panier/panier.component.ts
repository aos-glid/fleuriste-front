import {Component, OnInit} from '@angular/core';
import {PanierService} from '../../../services/panier.service';
import {Order} from '../../../models/order';
import {AuthService} from '../../../services/auth.service';
import {CommandeService} from '../../../services/commande.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  orders: Order[] = [];

  constructor(private panierService: PanierService, private authService: AuthService, private commandService: CommandeService,private router:Router) {
  }

  ngOnInit(): void {
    if (this.authService.currentUser.value != null) {
      this.panierService.getPanier(this.authService.currentUser.value.id).subscribe((data) => {
        this.orders = data.orders;
      });
    }
  }

  acheter() {
    if (this.orders.length != 0) {
      let commande = {idClient: this.authService.currentUser.value.id, orders: this.orders};
      this.commandService.add(commande).subscribe((data) => {
        this.panierService.currentPanier.next({id: this.authService.currentUser.value.id, orders: []});
        this.router.navigate(['/']);
      });
    }
  }
}
