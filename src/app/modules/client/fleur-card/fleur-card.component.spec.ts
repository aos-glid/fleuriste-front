import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FleurCardComponent } from './fleur-card.component';

describe('FleurCardComponent', () => {
  let component: FleurCardComponent;
  let fixture: ComponentFixture<FleurCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FleurCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FleurCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
