import {Component, Input, OnInit} from '@angular/core';
import {Fleur} from '../../../models/fleur';

@Component({
  selector: 'app-fleur-card',
  templateUrl: './fleur-card.component.html',
  styleUrls: ['./fleur-card.component.scss']
})
export class FleurCardComponent implements OnInit {


  @Input()
  public fleur:Fleur;
  constructor() { }

  ngOnInit(): void {
  }

}
