import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Fleur} from '../../../models/fleur';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {FleurService} from '../../../services/fleur.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {Commande} from '../../../models/commande';
import {CommandeService} from '../../../services/commande.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-commandes',
  templateUrl: './commandes.component.html',
  styleUrls: ['./commandes.component.scss']
})
export class CommandesComponent implements OnInit {


  displayedColumns: string[] = ['code','date', 'client', 'orders'];
  dataSource: MatTableDataSource<Commande>;
  public commandes: Commande[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private commandService: CommandeService,private authService:AuthService ,private router: Router) {
  }

  ngOnInit() {
    this.commandService.findAll().subscribe(
      (data) => {
        this.commandes = data;
        console.log(data);
        this.commandes.map((value => {
          this.authService.get(value.idClient).subscribe((client)=>{
              value.client=client.firstName+" "+client.lastName;
          });
        }))
        this.dataSource = new MatTableDataSource(this.commandes);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
